package com.demo.controller;

import com.demo.configuration.CustomUserDetails;
import com.demo.configuration.jwt.JwtTokenProvider;
import com.demo.dao.UserDAO;
import com.demo.dto.FullUserDTO;
import com.demo.dto.UserDTO;
import com.demo.mapper.UserMapper;
import com.demo.model.User;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    UserDAO userDAO;

    UserMapper userMapper;

    @RequestMapping("")
    public List<UserDTO> index() {
        return userMapper.INSTANCE.usersToUserDTOs(userDAO.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public UserDTO show(@PathVariable Integer id) {
        UserDTO userDTO = userMapper.INSTANCE.userToUserDTO(userDAO.findById(id).get());
        if (userDTO != null) {
            return userDTO;
        }
        return null;
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public UserDTO edit(@PathVariable Integer id) {
        return userMapper.INSTANCE.userToUserDTO(userDAO.findById(id).get());
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PATCH)
    public UserDTO update(@PathVariable Integer id, @Valid UserDTO userDTO) {
        User user = userDAO.findById(id).get();
        if(user != null) {
            userMapper.updateUserFromUserDTO(userDTO, user);
        }
        return userMapper.INSTANCE.userToUserDTO(userDAO.save(user));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Integer id) {
        User user = userDAO.findById(id).get();
        userDAO.delete(user);
    }
}
