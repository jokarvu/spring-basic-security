package com.demo.configuration;

import com.demo.dao.UserDAO;
import com.demo.dto.FullUserDTO;
import com.demo.mapper.FullUserMapper;
import com.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserDAO userDAO;

    @Override
    public UserDetails loadUserByUsername(String username) {
        FullUserDTO user = FullUserMapper.INSTANCE.userToUserDTO(userDAO.findByUsername(username));
        if(user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new CustomUserDetails(user);
    }

    @Transactional
    public UserDetails loadUserById(Integer id) {
        User user = userDAO.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + id)
        );
        FullUserDTO userDTO = FullUserMapper.INSTANCE.userToUserDTO(user);
        return new CustomUserDetails(userDTO);
    }
}
