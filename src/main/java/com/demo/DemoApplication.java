package com.demo;

import com.demo.dao.UserDAO;
import com.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserDAO userDAO;

    @Override
    public void run(String... args) throws Exception {
        User user = new User();
        user.setUsername("jokarvu");
        user.setEmail("jokarvu@gmail.com");
        user.setPassword(passwordEncoder.encode("1234"));
        userDAO.save(user);
    }
}
