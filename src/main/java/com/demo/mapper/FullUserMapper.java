package com.demo.mapper;

import com.demo.dto.FullUserDTO;
import com.demo.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface FullUserMapper {
    FullUserMapper INSTANCE = Mappers.getMapper(FullUserMapper.class);

    FullUserDTO userToUserDTO(User user);

    List<FullUserDTO> usersToUserDTOs(List<User> users);

    User userDTOToUser(FullUserDTO fullUserDTO);

    void updateUserFromUserDTO(FullUserDTO UserDTO, @MappingTarget User user);
}
