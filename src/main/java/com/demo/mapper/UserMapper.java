package com.demo.mapper;

import com.demo.dto.UserDTO;
import com.demo.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDTO userToUserDTO(User user);

    List<UserDTO> usersToUserDTOs(List<User> users);

    User userDTOToUser(UserDTO UserDTO);

    void updateUserFromUserDTO(UserDTO UserDTO, @MappingTarget User user);
}
